module.exports = (app, log, db, config, error, system, passport, validate, fs, form, adapter, logic, email, _, random, async, client, geoip, moment, ua, humanize) ->

	adminPath = config.adminPath

	app.all "#{adminPath}/audit", passport.ensureAuthenticated, (req, res, next) ->
		form.audit.create (f) ->
			param = {}
			if req.param("from") and req.param("to")
				_.extend param,
					from: req.param("from")
					to: req.param("to")

			logic.audit.list param, (err, result) ->
				if err
					log.error error[err]
					res.render "audit/list",
						message: error[err]
						session: req.session
						content: []
						form: f.toHTML()
						user: req.user

					return
				res.render "audit/list",
					content: result
					session: req.session
					form: f.toHTML()
					user: req.user



