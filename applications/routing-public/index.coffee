module.exports = (app, log, db, config, error, system, passport, validate, fs, form, adapter, logic, email, _, random, async, client, geoip, moment, ua, humanize, jobs) ->

	dir = config.templateDir

	app.get "/", (req, res, next) ->
		logic.post.list (err, result) ->
			if err
				log.error error[err]
				res.render "#{dir}/index",
					contents: []
				return
			res.render "#{dir}/index",
				contents: result

	app.get "/article/:slug", (req, res, next) ->
		logic.post.findBySlug req.param('slug'), (err, result) ->
			if err
				log.error err
				return res.redirect config.errorUrl
			if result.created_at
				result.created_at = moment(result.created_at).format('DD MMMM YYYY')
			res.render "#{dir}/article/single",
				contents: result
