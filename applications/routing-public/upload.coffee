crypto 		= require 'crypto'
module.exports = (app, log, db, config, error, system, passport, validate, fs, form, adapter, logic, email, _, random, async, client, geoip, moment, ua, humanize) ->

	allowedFormat = [ 'image/png', 'image/jpeg' ]
	maxSize = 1024 * 1024 * 3 #3MB 

	hash = (text) ->
		crypto.createHash('sha256').update(text + config.photoHash).digest('hex')

	getFolder = (folder) ->
		"#{__dirname}/../public-pic/#{folder}/"

	# TODO: heuristic checking to prevent file inclusion hack
	# TODO: checking if user already have pic, then do not upload
	app.post "/upload/certificate/:id", passport.ensureAuthenticated, (req, res, next) ->
		{ certificate } = req.files
		unless certificate
			return res.send 500,
				error: "file-empty"

		unless certificate.type in allowedFormat
			return res.send 500,
				error: "format-not-allowed"

		unless 1 < certificate.size <= maxSize
			return res.send 500,
				error: "size-too-big"

		file = hash("#{certificate.name}#{Date.now()}")
		id = hash req.param('id')
		folder = getFolder id

		if certificate.type is 'image/png'
			ext = '.png'
		else
			ext = '.jpg'
		file = "#{file}#{ext}"

		fs.mkdirs folder, (err) ->
			if err
				log.error "Failed to create folder. Error:#{err}. Path: #{folder}"

			fs.rename certificate.path, "#{folder}#{file}", (err) ->
				if err
					log.error "Failed to move file. Error:#{err}. Path: #{folder}#{file}"

				logic.certificate.savePic id, id, file, (err, updated) ->
					res.send 200, 
						result: 
							files: [
								name: 'abc'
							]