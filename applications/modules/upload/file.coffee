DB = require("../db").DB
crypto = require 'crypto'
module.exports = (app, log, dbase, config, error, system, crypto, _, uuid, fs, email, random, async, redis, geoip, moment, ua, url, humanize, jobs) ->
	db = new DB dbase, "test"

	hash = (text) ->
		crypto.createHash('sha256').update(text + config.photoHash).digest('hex')

	getFolder = (folder) ->
		"#{__dirname}/../../public-pic/#{folder}/"

	exports.getPath = (folder, file) ->
		folder = getFolder id
		"#{folder}#{file}"

	exports.upload = (f, id, cb) ->
		unless f
			return cb "file-empty"

		unless f.type in config.allowedFormat
			return cb "format-not-allowed"

		unless 1 < f.size <= config.maxSize
			return cb "size-too-big"

		file = hash("#{f.name}#{Date.now()}")
		id = hash id
		folder = getFolder id

		if f.type is 'image/png'
			ext = '.png'
		else
			ext = '.jpg'
		file = "#{file}#{ext}"

		fs.mkdirs folder, (err) ->
			if err
				log.error "Failed to create folder. Error:#{err}. Path: #{folder}"
				return cb 'failed-create'

			fs.rename f.path, "#{folder}#{file}", (err) ->
				if err
					log.error "Failed to move file. Error:#{err}. Path: #{folder}#{file}"
					return cb 'failed-move'

				cb undefined, 
					folder: id
					file: file

				# logic.certificate.savePic id, id, file, (err, updated) ->
				# 	res.send 200, 
				# 		result: 
				# 			files: [
				# 				name: 'abc'
				# 			]

	exports.uploadCV = (f, id, cb) ->
		unless f
			return cb "file-empty"

		file = hash("#{f.name}#{Date.now()}")
		id = hash id
		folder = getFolder id

		if f.type is 'application/zip'
			ext = '.zip'
		else
			ext = '.rar'

		file = "cv-#{file}#{ext}"

		fs.mkdirs folder, (err) ->
			if err
				log.error "Failed to create folder. Error:#{err}. Path: #{folder}"
				return cb 'failed-create'

			fs.rename f.path, "#{folder}#{file}", (err) ->
				if err
					log.error "Failed to move file. Error:#{err}. Path: #{folder}#{file}"
					return cb 'failed-move'

				cb undefined, 
					folder: id
					file: file

	exports