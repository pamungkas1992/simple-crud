DB = require("../db").DB
module.exports = (app, log, dbase, config, error, system, crypto, _, uuid, fs, email, random, async, redis, geoip, moment, ua, url, humanize, jobs) ->
	db = new DB dbase, "post"

	getUrl = (title) ->
		title = title.toLowerCase()
		result = ''
		if title.split(" ").length > 0
			title.split(" ").map (item) ->
				if result
					result = result + "-" + item
				else
					result = item
		result

	exports.list = (cb) ->
		db.findAll cb

	exports.findOne = (id, cb) ->
		try
			id = dbase.bson_serializer.ObjectID.createFromHexString id
		db.findOne { _id: id }, cb

	exports.findBySlug = (slug, cb) ->
		db.findOne { slug: slug }, cb

	exports.remove = (id, cb) ->
		try
			id = dbase.bson_serializer.ObjectID.createFromHexString id
		db.remove { _id: id }, cb

	exports.register = (data, cb) ->
		try
			id = dbase.bson_serializer.ObjectID.createFromHexString id

		if data.title
			data.slug = getUrl(data.title)

		if data.image
			_.extend data,
				image:
					folder: data.image.folder
					file: data.image.file

		db.findOne { slug: data.slug }, (err, find) =>
			unless find
				data.created_at = new Date()
				data.updated_at = new Date()
				db.insert data, (err, result) ->
					if err
						log.error err
						return cb err, false
					cb err, result
			else
				cb err, false

	exports.update = (id, data, cb) ->
		try
			id = dbase.bson_serializer.ObjectID.createFromHexString id

		if data.title
			data.slug = getUrl(data.title)

		if data.image
			_.extend data,
				image:
					folder: data.image.folder
					file: data.image.file

		data.updated_at = new Date()

		db.updateWithOption { _id: id }, { $set: data }, { upsert: false }, (err, updated) ->
			cb err, updated

	exports.remove = (id, cb) ->
		try
			id = dbase.bson_serializer.ObjectID.createFromHexString id
		db.remove { _id: id }, cb

	exports