module.exports = (app, log, dbase, config, error, system, crypto, _, uuid, fs, email, random, async, redis, geoip, moment, ua, url, humanize, jobs) ->
	
	exports.get = (locale, cb) ->
		fs.readFile "#{__dirname}/../../../locales/#{locale}.js", (err, content) ->
			log.error err if err
			cb err, content

	exports.save = (locale, content, cb) ->
		fs.writeFile "#{__dirname}/../../../locales/#{locale}.js.example", content, (err) ->
			fs.writeFile "#{__dirname}/../../../locales/#{locale}.js", content, (err) ->
				if err
					log.error err
				return cb err, true

	exports