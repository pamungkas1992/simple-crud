path = require("path")
supportFile = ["js", "coffee"]
module.exports = (app, log, db, config, error, system, crypto, _, uuid, fs, email, random, async, redis, geoip, moment, ua, url, humanize, jobs) ->
    ["admin", "locale", "upload", "user", "post" ].forEach (folder) ->
        files = fs.readdirSync(path.join(__dirname, folder))
        files.forEach (file) ->
            return  unless _.contains(supportFile, file.split(".")[1])
            exports[file.replace(/\.js$/, "").replace(/\.coffee$/, "")] = require(path.join(__dirname, folder, file.replace(/\.js$/, "").replace(/\.coffee$/, "")))(app, log, db, config, error, system, crypto, _, uuid, fs, email, random, async, redis, geoip, moment, ua, url, humanize, jobs)
    exports