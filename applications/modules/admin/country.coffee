countries = [
	name: "Afghanistan"
	code: "AF"
,
	name: "Åland Islands"
	code: "AX"
,
	name: "Albania"
	code: "AL"
,
	name: "Algeria"
	code: "DZ"
,
	name: "American Samoa"
	code: "AS"
,
	name: "AndorrA"
	code: "AD"
,
	name: "Angola"
	code: "AO"
,
	name: "Anguilla"
	code: "AI"
,
	name: "Antarctica"
	code: "AQ"
,
	name: "Antigua and Barbuda"
	code: "AG"
,
	name: "Argentina"
	code: "AR"
,
	name: "Armenia"
	code: "AM"
,
	name: "Aruba"
	code: "AW"
,
	name: "Australia"
	code: "AU"
,
	name: "Austria"
	code: "AT"
,
	name: "Azerbaijan"
	code: "AZ"
,
	name: "Bahamas"
	code: "BS"
,
	name: "Bahrain"
	code: "BH"
,
	name: "Bangladesh"
	code: "BD"
,
	name: "Barbados"
	code: "BB"
,
	name: "Belarus"
	code: "BY"
,
	name: "Belgium"
	code: "BE"
,
	name: "Belize"
	code: "BZ"
,
	name: "Benin"
	code: "BJ"
,
	name: "Bermuda"
	code: "BM"
,
	name: "Bhutan"
	code: "BT"
,
	name: "Bolivia"
	code: "BO"
,
	name: "Bosnia and Herzegovina"
	code: "BA"
,
	name: "Botswana"
	code: "BW"
,
	name: "Bouvet Island"
	code: "BV"
,
	name: "Brazil"
	code: "BR"
,
	name: "British Indian Ocean Territory"
	code: "IO"
,
	name: "Brunei Darussalam"
	code: "BN"
,
	name: "Bulgaria"
	code: "BG"
,
	name: "Burkina Faso"
	code: "BF"
,
	name: "Burundi"
	code: "BI"
,
	name: "Cambodia"
	code: "KH"
,
	name: "Cameroon"
	code: "CM"
,
	name: "Canada"
	code: "CA"
,
	name: "Cape Verde"
	code: "CV"
,
	name: "Cayman Islands"
	code: "KY"
,
	name: "Central African Republic"
	code: "CF"
,
	name: "Chad"
	code: "TD"
,
	name: "Chile"
	code: "CL"
,
	name: "China"
	code: "CN"
,
	name: "Christmas Island"
	code: "CX"
,
	name: "Cocos (Keeling) Islands"
	code: "CC"
,
	name: "Colombia"
	code: "CO"
,
	name: "Comoros"
	code: "KM"
,
	name: "Congo"
	code: "CG"
,
	name: "Congo, The Democratic Republic of the"
	code: "CD"
,
	name: "Cook Islands"
	code: "CK"
,
	name: "Costa Rica"
	code: "CR"
,
	name: "Cote D'Ivoire"
	code: "CI"
,
	name: "Croatia"
	code: "HR"
,
	name: "Cuba"
	code: "CU"
,
	name: "Cyprus"
	code: "CY"
,
	name: "Czech Republic"
	code: "CZ"
,
	name: "Denmark"
	code: "DK"
,
	name: "Djibouti"
	code: "DJ"
,
	name: "Dominica"
	code: "DM"
,
	name: "Dominican Republic"
	code: "DO"
,
	name: "Ecuador"
	code: "EC"
,
	name: "Egypt"
	code: "EG"
,
	name: "El Salvador"
	code: "SV"
,
	name: "Equatorial Guinea"
	code: "GQ"
,
	name: "Eritrea"
	code: "ER"
,
	name: "Estonia"
	code: "EE"
,
	name: "Ethiopia"
	code: "ET"
,
	name: "Falkland Islands (Malvinas)"
	code: "FK"
,
	name: "Faroe Islands"
	code: "FO"
,
	name: "Fiji"
	code: "FJ"
,
	name: "Finland"
	code: "FI"
,
	name: "France"
	code: "FR"
,
	name: "French Guiana"
	code: "GF"
,
	name: "French Polynesia"
	code: "PF"
,
	name: "French Southern Territories"
	code: "TF"
,
	name: "Gabon"
	code: "GA"
,
	name: "Gambia"
	code: "GM"
,
	name: "Georgia"
	code: "GE"
,
	name: "Germany"
	code: "DE"
,
	name: "Ghana"
	code: "GH"
,
	name: "Gibraltar"
	code: "GI"
,
	name: "Greece"
	code: "GR"
,
	name: "Greenland"
	code: "GL"
,
	name: "Grenada"
	code: "GD"
,
	name: "Guadeloupe"
	code: "GP"
,
	name: "Guam"
	code: "GU"
,
	name: "Guatemala"
	code: "GT"
,
	name: "Guernsey"
	code: "GG"
,
	name: "Guinea"
	code: "GN"
,
	name: "Guinea-Bissau"
	code: "GW"
,
	name: "Guyana"
	code: "GY"
,
	name: "Haiti"
	code: "HT"
,
	name: "Heard Island and Mcdonald Islands"
	code: "HM"
,
	name: "Holy See (Vatican City State)"
	code: "VA"
,
	name: "Honduras"
	code: "HN"
,
	name: "Hong Kong"
	code: "HK"
,
	name: "Hungary"
	code: "HU"
,
	name: "Iceland"
	code: "IS"
,
	name: "India"
	code: "IN"
,
	name: "Indonesia"
	code: "ID"
,
	name: "Iran, Islamic Republic Of"
	code: "IR"
,
	name: "Iraq"
	code: "IQ"
,
	name: "Ireland"
	code: "IE"
,
	name: "Isle of Man"
	code: "IM"
,
	name: "Israel"
	code: "IL"
,
	name: "Italy"
	code: "IT"
,
	name: "Jamaica"
	code: "JM"
,
	name: "Japan"
	code: "JP"
,
	name: "Jersey"
	code: "JE"
,
	name: "Jordan"
	code: "JO"
,
	name: "Kazakhstan"
	code: "KZ"
,
	name: "Kenya"
	code: "KE"
,
	name: "Kiribati"
	code: "KI"
,
	name: "Korea, Democratic People'S Republic of"
	code: "KP"
,
	name: "Korea, Republic of"
	code: "KR"
,
	name: "Kuwait"
	code: "KW"
,
	name: "Kyrgyzstan"
	code: "KG"
,
	name: "Lao People'S Democratic Republic"
	code: "LA"
,
	name: "Latvia"
	code: "LV"
,
	name: "Lebanon"
	code: "LB"
,
	name: "Lesotho"
	code: "LS"
,
	name: "Liberia"
	code: "LR"
,
	name: "Libyan Arab Jamahiriya"
	code: "LY"
,
	name: "Liechtenstein"
	code: "LI"
,
	name: "Lithuania"
	code: "LT"
,
	name: "Luxembourg"
	code: "LU"
,
	name: "Macao"
	code: "MO"
,
	name: "Macedonia, The Former Yugoslav Republic of"
	code: "MK"
,
	name: "Madagascar"
	code: "MG"
,
	name: "Malawi"
	code: "MW"
,
	name: "Malaysia"
	code: "MY"
,
	name: "Maldives"
	code: "MV"
,
	name: "Mali"
	code: "ML"
,
	name: "Malta"
	code: "MT"
,
	name: "Marshall Islands"
	code: "MH"
,
	name: "Martinique"
	code: "MQ"
,
	name: "Mauritania"
	code: "MR"
,
	name: "Mauritius"
	code: "MU"
,
	name: "Mayotte"
	code: "YT"
,
	name: "Mexico"
	code: "MX"
,
	name: "Micronesia, Federated States of"
	code: "FM"
,
	name: "Moldova, Republic of"
	code: "MD"
,
	name: "Monaco"
	code: "MC"
,
	name: "Mongolia"
	code: "MN"
,
	name: "Montserrat"
	code: "MS"
,
	name: "Morocco"
	code: "MA"
,
	name: "Mozambique"
	code: "MZ"
,
	name: "Myanmar"
	code: "MM"
,
	name: "Namibia"
	code: "NA"
,
	name: "Nauru"
	code: "NR"
,
	name: "Nepal"
	code: "NP"
,
	name: "Netherlands"
	code: "NL"
,
	name: "Netherlands Antilles"
	code: "AN"
,
	name: "New Caledonia"
	code: "NC"
,
	name: "New Zealand"
	code: "NZ"
,
	name: "Nicaragua"
	code: "NI"
,
	name: "Niger"
	code: "NE"
,
	name: "Nigeria"
	code: "NG"
,
	name: "Niue"
	code: "NU"
,
	name: "Norfolk Island"
	code: "NF"
,
	name: "Northern Mariana Islands"
	code: "MP"
,
	name: "Norway"
	code: "NO"
,
	name: "Oman"
	code: "OM"
,
	name: "Pakistan"
	code: "PK"
,
	name: "Palau"
	code: "PW"
,
	name: "Palestinian Territory, Occupied"
	code: "PS"
,
	name: "Panama"
	code: "PA"
,
	name: "Papua New Guinea"
	code: "PG"
,
	name: "Paraguay"
	code: "PY"
,
	name: "Peru"
	code: "PE"
,
	name: "Philippines"
	code: "PH"
,
	name: "Pitcairn"
	code: "PN"
,
	name: "Poland"
	code: "PL"
,
	name: "Portugal"
	code: "PT"
,
	name: "Puerto Rico"
	code: "PR"
,
	name: "Qatar"
	code: "QA"
,
	name: "Reunion"
	code: "RE"
,
	name: "Romania"
	code: "RO"
,
	name: "Russian Federation"
	code: "RU"
,
	name: "RWANDA"
	code: "RW"
,
	name: "Saint Helena"
	code: "SH"
,
	name: "Saint Kitts and Nevis"
	code: "KN"
,
	name: "Saint Lucia"
	code: "LC"
,
	name: "Saint Pierre and Miquelon"
	code: "PM"
,
	name: "Saint Vincent and the Grenadines"
	code: "VC"
,
	name: "Samoa"
	code: "WS"
,
	name: "San Marino"
	code: "SM"
,
	name: "Sao Tome and Principe"
	code: "ST"
,
	name: "Saudi Arabia"
	code: "SA"
,
	name: "Senegal"
	code: "SN"
,
	name: "Serbia and Montenegro"
	code: "CS"
,
	name: "Seychelles"
	code: "SC"
,
	name: "Sierra Leone"
	code: "SL"
,
	name: "Singapore"
	code: "SG"
,
	name: "Slovakia"
	code: "SK"
,
	name: "Slovenia"
	code: "SI"
,
	name: "Solomon Islands"
	code: "SB"
,
	name: "Somalia"
	code: "SO"
,
	name: "South Africa"
	code: "ZA"
,
	name: "South Georgia and the South Sandwich Islands"
	code: "GS"
,
	name: "Spain"
	code: "ES"
,
	name: "Sri Lanka"
	code: "LK"
,
	name: "Sudan"
	code: "SD"
,
	name: "Suriname"
	code: "SR"
,
	name: "Svalbard and Jan Mayen"
	code: "SJ"
,
	name: "Swaziland"
	code: "SZ"
,
	name: "Sweden"
	code: "SE"
,
	name: "Switzerland"
	code: "CH"
,
	name: "Syrian Arab Republic"
	code: "SY"
,
	name: "Taiwan, Province of China"
	code: "TW"
,
	name: "Tajikistan"
	code: "TJ"
,
	name: "Tanzania, United Republic of"
	code: "TZ"
,
	name: "Thailand"
	code: "TH"
,
	name: "Timor-Leste"
	code: "TL"
,
	name: "Togo"
	code: "TG"
,
	name: "Tokelau"
	code: "TK"
,
	name: "Tonga"
	code: "TO"
,
	name: "Trinidad and Tobago"
	code: "TT"
,
	name: "Tunisia"
	code: "TN"
,
	name: "Turkey"
	code: "TR"
,
	name: "Turkmenistan"
	code: "TM"
,
	name: "Turks and Caicos Islands"
	code: "TC"
,
	name: "Tuvalu"
	code: "TV"
,
	name: "Uganda"
	code: "UG"
,
	name: "Ukraine"
	code: "UA"
,
	name: "United Arab Emirates"
	code: "AE"
,
	name: "United Kingdom"
	code: "GB"
,
	name: "United States"
	code: "US"
,
	name: "United States Minor Outlying Islands"
	code: "UM"
,
	name: "Uruguay"
	code: "UY"
,
	name: "Uzbekistan"
	code: "UZ"
,
	name: "Vanuatu"
	code: "VU"
,
	name: "Venezuela"
	code: "VE"
,
	name: "Viet Nam"
	code: "VN"
,
	name: "Virgin Islands, British"
	code: "VG"
,
	name: "Virgin Islands, U.S."
	code: "VI"
,
	name: "Wallis and Futuna"
	code: "WF"
,
	name: "Western Sahara"
	code: "EH"
,
	name: "Yemen"
	code: "YE"
,
	name: "Zambia"
	code: "ZM"
,
	name: "Zimbabwe"
	code: "ZW"
]
regions =
	Bali: ["Badung", "Bangli", "Buleleng", "Denpasar (Kota)", "Gianyar", "Jembrana", "Karangasem", "Klungkung", "Tabanan"]
	"Bangka Belitung": ["Bangka", "Bangka Barat", "Bangka Selatan", "Bangka Tengah", "Belitung", "Belitung Timur", "Pangkal Pinang (Kota)"]
	Banten: ["Cilegon (Kota)", "Lebak", "Pandeglang", "Serang (Kabupaten)", "Serang (Kota)", "Tangerang (Kabupaten)", "Tangerang (Kota)", "Tangerang Selatan"]
	Bengkulu: ["Bengkulu (Kota)", "Bengkulu Selatan", "Bengkulu Tengah", "Bengkulu Utara", "Kaur", "Kepahiang", "Lebong", "Muko Muko", "Rejang Lebong", "Seluma"]
	"D.I Yogyakarta": ["Bantul", "Gunung Kidul", "Kulon Progo", "Sleman", "Yogyakarta (Kota)"]
	"DKI Jakarta": ["Jakarta Barat", "Jakarta Pusat", "Jakarta Selatan", "Jakarta Timur", "Jakarta Utara", "Kepulauan Seribu"]
	Gorontalo: ["Boalemo", "Bone Bolango", "Gorontalo (Kabupaten)", "Gorontalo (Kota)", "Gorontalo Utara", "Pohuwato"]
	Jambi: ["Batang Hari", "Bungo", "Jambi (Kota)", "Kerinci", "Merangin", "Muaro Jambi", "Sarolangun", "Sungaipenuh", "Tanjung Jabung Barat", "Tanjung Jabung Timur", "Tebo"]
	"Jawa Barat": ["Bandung (Kabupaten)", "Bandung (Kota)", "Bandung Barat", "Banjar (Kota)", "Bekasi (Kabupaten)", "Bekasi (Kota)", "Bogor (Kabupaten)", "Bogor (Kota)", "Ciamis", "Cianjur", "Cimahi", "Cirebon (Kabupaten)", "Cirebon (Kota)", "Depok", "Garut", "Indramayu", "Karawang", "Kuningan", "Majalengka", "Pangandaran", "Purwakarta", "Subang", "Sukabumi (Kabupaten)", "Sukabumi (Kota)", "Sumedang", "Tasikmalaya (Kabupaten)", "Tasikmalaya (Kota)"]
	"Jawa Tengah": ["Banjarnegara", "Banyumas", "Batang", "Blora", "Boyolali", "Brebes", "Cilacap", "Demak", "Grobogan", "Jepara", "Karanganyar", "Kebumen", "Kendal", "Klaten", "Kudus", "Magelang (Kabupaten)", "Magelang (Kota)", "Pati", "Pekalongan (Kabupaten)", "Pekalongan (Kota)", "Pemalang", "Purbalingga", "Purworejo", "Rembang", "Salatiga (Kota)", "Semarang (Kabupaten)", "Semarang (Kota)", "Sragen", "Sukoharjo", "Surakarta/Solo", "Tegal (Kabupaten)", "Tegal (Kota)", "Temanggung", "Wonogiri", "Wonosobo"]
	"Jawa Timur": ["Bangkalan", "Banyuwangi", "Batu (Kota)", "Blitar (Kabupaten)", "Blitar (Kota)", "Bojonegoro", "Bondowoso", "Gresik", "Jember", "Jombang", "Kediri (Kabupaten)", "Kediri (Kota)", "Lamongan", "Lumajang", "Madiun (Kabupaten)", "Madiun (Kota)", "Magetan", "Malang (Kabupaten)", "Malang (Kota)", "Mojokerto (Kabupaten)", "Mojokerto (Kota)", "Nganjuk", "Ngawi", "Pacitan", "Pamekasan", "Pasuruan (Kabupaten)", "Pasuruan (Kota)", "Ponorogo", "Probolinggo (Kabupaten)", "Probolinggo (Kota)", "Sampang", "Sidoarjo", "Situbondo", "Sumenep", "Surabaya (Kota)", "Trenggalek", "Tuban", "Tulungagung"]
	"Kalimantan Barat": ["Bengkayang", "Kapuas Hulu", "Kayong Utara", "Ketapang", "Kubu Raya", "Landak", "Melawi", "Pontianak (Kabupaten)", "Pontianak (Kota)", "Sambas", "Sanggau", "Sekadau", "Singkawang (Kota)", "Sintang"]
	"Kalimantan Selatan": ["Balangan", "Banjar", "Banjarbaru (Kota)", "Banjarmasin (Kota)", "Barito Kuala", "Hulu Sungai Selatan", "Hulu Sungai Tengah", "Hulu Sungai Utara", "Kotabaru", "Tabalong", "Tanah Bumbu", "Tanah Laut", "Tapin"]
	"Kalimantan Tengah": ["Barito Selatan", "Barito Timur", "Barito Utara", "Gunung Mas", "Kapuas", "Katingan", "Kotawaringin Barat", "Kotawaringin Timur", "Lamandau", "Murung Raya", "Palangka Raya (Kota)", "Pulang Pisau", "Seruyan", "Sukamara"]
	"Kalimantan Timur": ["Balikpapan (Kota)", "Berau", "Bontang (Kota)", "Kutai Barat", "Kutai Kartanegara", "Kutai Timur", "Paser", "Penajam Paser Utara", "Samarinda (Kota)"]
	"Kalimantan Utara": ["Bulungan/Bulongan", "Malinau", "Nunukan", "Tana Tidung", "Tarakan (Kota)"]
	"Kepulauan Riau": ["Batam (Kota)", "Bintan", "Karimun", "Kepulauan Anambas", "Lingga", "Natuna", "Tanjung Pinang (Kota)"]
	Lampung: ["Bandar Lampung (Kota)", "Lampung Barat", "Lampung Selatan", "Lampung Tengah", "Lampung Timur", "Lampung Utara", "Mesuji", "Metro (Kota)", "Pesawaran", "Pringsewu", "Tanggamus", "Tulang Bawang", "Tulang Bawang Barat", "Way Kanan", "Pesisir Barat"]
	Maluku: ["Ambon (Kota)", "Buru", "Buru Selatan", "Kepulauan Aru", "Maluku Barat Daya", "Maluku Tengah", "Maluku Tenggara", "Maluku Tenggara Barat", "Seram Bagian Barat", "Seram Bagian Timur", "Tual (Kota)"]
	"Maluku Utara": ["Halmahera Barat", "Halmahera Selatan", "Halmahera Tengah", "Halmahera Timur", "Halmahera Utara", "Kepulauan Sula", "Pulau Morotai", "Ternate (Kota)", "Tidore Kepulauan (Kota)"]
	"Nanggroe Aceh Darussalam": ["Aceh Barat", "Aceh Barat Daya", "Aceh Besar", "Aceh Jaya", "Aceh Selatan", "Aceh Singkil", "Aceh Tamiang", "Aceh Tengah", "Aceh Tenggara", "Aceh Timur", "Aceh Utara", "Banda Aceh (Kota)", "Bener Meriah", "Bireuen", "Gayo Lues", "Langsa (Kota)", "Lhokseumawe (Kota)", "Nagan Raya", "Pidie", "Pidie Jaya", "Sabang (Kota)", "Simeulue", "Subulussalam (Kota)"]
	"Nusa Tenggara Barat": ["Bima (Kabupaten)", "Bima (Kota)", "Dompu", "Lombok Barat", "Lombok Tengah", "Lombok Timur", "Lombok Utara", "Mataram (Kota)", "Sumbawa", "Sumbawa Barat"]
	"Nusa Tenggara Timur": ["Alor", "Belu", "Ende", "Flores Timur", "Kupang (Kabupaten)", "Kupang (Kota)", "Lembata", "Manggarai", "Manggarai Barat", "Manggarai Timur", "Nagekeo", "Ngada", "Rote Ndao", "Sabu Raijua", "Sikka", "Sumba Barat", "Sumba Barat Daya", "Sumba Tengah", "Sumba Timur", "Timor Tengah Selatan", "Timor Tengah Utara"]
	Papua: ["Asmat", "Biak Numfor", "Boven Digoel", "Deiyai", "Dogiyai", "Intan Jaya", "Jayapura (Kabupaten)", "Jayapura (Kota)", "Jayawijaya", "Keerom", "Kepulauan Yapen", "Lanny Jaya", "Mappi", "Mamberamo Raya", "Mamberamo Tengah", "Merauke", "Mimika", "Nabire", "Nduga", "Paniai", "Pegunungan Bintang", "Puncak", "Puncak Jaya", "Sarmi", "Supiori", "Tolikara", "Waropen", "Yahukimo", "Yalimo"]
	"Papua Barat": ["Fak Fak", "Kaimana", "Manokwari", "Manokwari Selatan", "Maybrat", "Pegunungan Arfak", "Raja Ampat", "Sorong (Kabupaten)", "Sorong (Kota)", "Sorong Selatan", "Tambrauw", "Teluk Bintuni", "Teluk Wondama"]
	Riau: ["Bengkalis", "Dumai (Kota)", "Indragiri Hilir", "Indragiri Hulu", "Kampar", "Kepulauan Meranti", "Kuantan Singingi", "Pekanbaru (Kota)", "Pelalawan", "Rokan Hilir", "Rokan Hulu", "Siak"]
	"Sulawesi Barat": ["Majene", "Mamasa", "Mamuju", "Mamuju Tengah", "Mamuju Utara", "Polewali Mandar"]
	"Sulawesi Selatan": ["Bantaeng", "Barru", "Bone", "Bulukumba", "Enrekang", "Gowa", "Jeneponto", "Luwu", "Luwu Timur", "Luwu Utara", "Makassar (Kota)", "Maros", "Palopo (Kota)", "Pangkajene Kepulauan", "Parepare (Kota)", "Pinrang", "Selayar/Kepulauan Selayar", "Sidenreng Rappang / Rapang", "Sinjai", "Soppeng", "Takalar", "Tana Toraja", "Toraja Utara", "Wajo"]
	"Sulawesi Tengah": ["Banggai", "Banggai Kepulauan", "Buol", "Donggala", "Morowali", "Palu (Kota)", "Parigi Moutong", "Poso", "Sigi", "Tojo Una-Una", "Toli-Toli"]
	"Sulawesi Tenggara": ["Bau-Bau (Kota)", "Bombana", "Buton", "Buton Utara", "Kendari (Kota)", "Kolaka", "Kolaka Timur", "Kolaka Utara", "Konawe", "Konawe Selatan", "Konawe Utara", "Muna", "Wakatobi"]
	"Sulawesi Utara": ["Bitung (Kota)", "Bolaang Mongondow/Bolmong", "Bolaang Mongondow Selatan", "Bolaang Mongondow Timur", "Bolaang Mongondow Utara", "Kepulauan Talaud", "Kepulauan Sangihe", "Kepulauan Siau Tagulandang Biaro/Sitaro", "Kotamobagu (Kota)", "Manado (Kota)", "Minahasa", "Minahasa Selatan", "Minahasa Tenggara", "Minahasa Utara", "Tomohon (Kota)"]
	"Sumatera Barat": ["Agam", "Bukittinggi (Kota)", "Dharmas Raya", "Kepulauan Mentawai", "Lima Puluh Koto/Kota", "Padang (Kota)", "Padang Panjang (Kota)", "Padang Pariaman", "Pariaman (Kota)", "Pasaman", "Pasaman Barat", "Payakumbuh (Kota)", "Pesisir Selatan", "Sawah Lunto (Kota)", "Sijunjung/Sawah Lunto Sijunjung", "Solok (Kabupaten)", "Solok (Kota)", "Solok Selatan", "Tanah Datar"]
	"Sumatera Selatan": ["Banyuasin", "Empat Lawang", "Lahat", "Lubuk Linggau (Kota)", "Muara Enim", "Musi Banyuasin", "Musi Rawas", "Ogan Ilir", "Ogan Komering Ilir", "Ogan Komering Ulu", "Ogan Komering Ulu Selatan", "Ogan Komering Ulu Timur", "Pagar Alam (Kota)", "Palembang (Kota)", "Penukal Abab Lematang Ilir", "Prabumulih (Kota)"]
	"Sumatera Utara": ["Asahan", "Batu Bara", "Binjai (Kota)", "Dairi", "Deli Serdang", "Gunung Sitoli", "Humbang Hasundutan", "Karo", "Labuhan Batu", "Labuhan Batu Selatan", "Labuhan Batu Utara", "Langkat", "Mandailing Natal", "Medan (Kota)", "Nias", "Nias Barat", "Nias Selatan", "Nias Utara", "Padang Lawas", "Padang Lawas Utara", "Padang Sidempuan (Kota)", "Pak pak Bharat", "Pematang Siantar (Kota)", "Samosir", "Serdang Bedagai", "Sibolga (Kota)", "Simalungun", "Tanjung Balai (Kota)", "Tapanuli Selatan", "Tapanuli Tengah", "Tapanuli Utara", "Tapauli Tengah", "Tebing Tinggi (Kota)", "Toba Samosir"]

module.exports = (app, log, dbase, config, error, system, crypto, _, uuid, fs, email, random, async, redis, geoip, moment, ua, url, humanize, jobs) ->

	cities = []

	_.keys(regions).map (items) ->
		# cities = cities.concat 
		regions[items].map (city) ->
			cities.push "#{city} - #{items}"

	exports.getCity = () ->
		cities

	exports.getCountry = () ->
		_.pluck countries, 'name'

	exports