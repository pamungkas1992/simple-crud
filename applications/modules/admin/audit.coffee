DB = require("../collDb").DB
module.exports = (app, log, dbase, config, error, system, crypto, _, uuid, fs, email, random, async, redis, geoip, moment, ua, url, humanize, jobs) ->
	db = new DB(dbase, "audit")

	exports.create = (param, cb) ->
		param._id = random.token(20)
		_.extend param,
			created_date: new Date()

		db.insert param, (err, docs) ->
			if err
				log.error err
				cb 203
				return
			cb null, true


	exports.list = (param, cb) ->
		if param.from and param.to
			start = new Date(param.from.substring(6, 10), param.from.substring(3, 5) - 1, param.from.substring(0, 2))
			end = new Date(param.to.substring(6, 10), param.to.substring(3, 5) - 1, param.to.substring(0, 2))
			delete param.from
			delete param.to
			_.extend param,
				logTime:
					$gte: start
					$lte: end

		db.find param, (err, customers) ->
			if err or not customers
				log.error err
				return cb 207
			cb null, customers


	exports