DB = require("../db").DB
module.exports = (app, log, dbase, config, error, system, crypto, _, uuid, fs, email, random, async, redis, geoip, moment, ua, url, humanize, jobs) ->
	db = new DB(dbase, "config")

	exports.get = (cb) ->
		param = 
			_id: '1'
		db.findOne { _id: '1' }, cb

	exports.getConfig = (cb) ->
		param = 
			_id: '1'
			isDelete: false
		db.findOne param, (err, res) ->
			return cb err if err
			try
				metadata = JSON.parse res.metadata
			catch e
				metadata = {}
			cb null, metadata

	exports.save = (metadata, cb) ->
		db.updateWithOption { _id: '1' }, { $set: { metadata: metadata, isDelete: false } }, { upsert: true }, (err, config) ->
			if err or not config
				log.error err
				cb 207
				return
			cb null, config

	exports