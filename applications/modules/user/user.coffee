DB = require("../db").DB
module.exports = (app, log, dbase, config, error, system, crypto, _, uuid, fs, email, random, async, redis, geoip, moment, ua, url, humanize, jobs) ->
	db = new DB dbase, "user"
	
	exports.get = (id, cb) ->
		try
			id = dbase.bson_serializer.ObjectID.createFromHexString id
		db.findOne { _id: id }, cb
	
	exports.list = (cb) ->
		db.findAll cb
		
	exports.findMail = (data, cb) ->
		db.findOne { email: data.email }, cb
		
	exports.findOne = (id, cb) ->
		try
			id = dbase.bson_serializer.ObjectID.createFromHexString id
		db.findOne { _id: id }, cb

	exports.authentication = (param, cb) ->
		db.findOne param, cb

	exports.savePic = (id, folder, file, cb) ->
		try
			id = dbase.bson_serializer.ObjectID.createFromHexString id
		updatedData = 
			pic:
				folder: folder
				file: file
		db.updateWithOption { _id: id }, { $set: updatedData }, { upsert: false }, (err, updated) ->
			cb err, updated

	exports.register = (param, cb) ->
		user =
			name: param.name
			email: param.email
			password: crypto.createHash("sha256").update(param.password).digest("hex")
			status: system.user.status.active
			date: new Date()
			host: param.host

		db.findOne { email: param.email }, (err, exist) ->
			if err or exist
				return cb 'user-is-exist'
			db.insert user, (err, usr) ->
				if err
					log.error err
					return cb 203
				_.extend user, 
					title: "#{param.name} - #{param.email}"
				jobs.create('signup-email', user).priority('high').attempts(5).save()
				cb null, true

	exports.update = (id, data, cb) ->
		try
			id = dbase.bson_serializer.ObjectID.createFromHexString id
		user =
			name: data.name
			email: data.email
			update_date: new Date()
		if data.password
			_.extend user,
				password: crypto.createHash("sha256").update(data.password).digest("hex") 
		db.updateWithOption { _id: id }, { $set: user }, { upsert: false }, (err, updated) ->
			cb err, updated

	
	exports.forget = (userEmail, cb) ->
		expired = 60 * 60 * 24 #24 hour
		token = random.token(35)
		key = "applicationName:forget:#{token}"
		console.dir userEmail
		db.findOne { email: userEmail }, (err, user) ->
			if err
				log.error err
				return cb 'page.forget.error.general'
			unless user
				return cb 'page.forget.error.email.notExist'

			redis.setex key, expired, user.email, (err) ->
				if err
					log.error err
					return cb 'page.forget.error.general'
				
				_.extend user, 
					title: "#{user.name} - #{user.email}"
					token: token
				jobs.create('forgot-email', user).priority('high').attempts(5).save()

				cb null, 'page.forget.success'


	exports.findForget = (token, cb) ->
		key = "applicationName:forget:#{token}"
		redis.get key, (err, res) ->
			if err
				log.error err
				return cb 'page.forget.error.general'
			unless res
				return cb 'page.forget.error.token.notExist'
			db.findOne { email: res }, (err, user) ->
				if err
					log.error err
					return cb 'page.forget.error.general'
				unless user
					return cb 'page.forget.error.token.notExist'
				cb null, user

	exports.changePassword = (token, userEmail, password, cb) ->
		key = "applicationName:forget:#{token}"
		dbase.user.update
			email: userEmail
		,
			$set:
				password: crypto.createHash("sha256").update(password).digest("hex")
		, (err, result) ->
			if err
				log.error err
				return cb 'forget-error-general'
			redis.del key, (err) ->
				cb null, 'forget-update-success'
	exports.remove = (id, cb) ->
		try
			id = dbase.bson_serializer.ObjectID.createFromHexString id
		db.remove { _id: id }, cb

	exports