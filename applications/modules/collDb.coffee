_ = require("underscore")

DB = (db, dbName) ->
	@db = db
	@dbName = dbName

DB:: =
	getDb: ->
		@db.collection @dbName

	insert: (obj, opt, cb) ->
		cb 201  unless obj
		if not cb and opt
			cb = opt
			opt = {}
		_.extend opt,
			safe: true

		@getDb().insert obj, opt, (err, u) ->
			cb err, u


	save: (obj, opt, cb) ->
		cb 201  unless obj
		if not cb and opt
			cb = opt
			opt = {}
		_.extend opt,
			safe: true
			new: true

		@getDb().save obj, opt, (err, u) ->
			cb err, u


	update: (query, newData, cb) ->
		cb 201  unless query
		if not cb and newData
			cb = newData
			newData = {}
		_.extend newData,
			safe: true

		@getDb().update query, newData, (err, u) ->
			cb err, true


	remove: (query, opt, cb) ->
		cb 202  unless query
		if not cb and opt
			cb = opt
			opt = {}
		@getDb().remove query, (err, obj) ->
			cb err, true


	find: (param, cb) ->
		cb 202  unless param
		@getDb().findItems param, cb

	findOne: (param, opt, cb) ->
		cb 202  unless param
		if not cb and opt
			cb = opt
			opt = {}
		@getDb().findOne param, (err, obj) ->
			cb err, obj


	findAll: (cb) ->
		@getDb().find().toArray (err, objs) ->
			cb err, objs

	findAndModify: (query, doc, opt, cb) ->
		cb 202  unless query
		if not cb and opt
			cb = opt
			opt = {}
		
		_.extend opt,
			safe: true
			new: true

		@getDb().findAndModify query, {}, doc, opt, (err, obj) ->
			cb err, obj


	findAllWithKey: (cb) ->
		@getDb().find().toArray (err, objs) ->
			keys = []
			_.each objs, (item) ->
				key = _.keys(item)
				keys = keys.concat(key)

			keys = _.uniq(keys)
			keys.sort()
			result =
				key: keys
				result: objs

			cb err, result


exports.DB = DB