_ = require("underscore")

DB = (db, dbName) ->
	@db = db
	@dbName = dbName

DB:: =
	getDb: ->
		@db.collection @dbName

	insert: (obj, opt, cb) ->
		cb 201  unless obj
		if not cb and opt
			cb = opt
			opt = {}
		_.extend opt,
			safe: true

		_.extend obj,
			isDelete: false

		@getDb().insert obj, opt, (err, u) ->
			cb err, u


	save: (obj, opt, cb) ->
		cb 201  unless obj
		if not cb and opt
			cb = opt
			opt = {}
		_.extend opt,
			safe: true

		_.extend obj,
			isDelete: false

		@getDb().save obj, opt, (err, u) ->
			cb err, u


	update: (query, newData, cb) ->
		cb 201  unless query
		if not cb and newData
			cb = newData
			newData = {}
		_.extend newData,
			safe: true

		@getDb().update query, newData, (err, u) ->
			cb err, true

	updateWithOption: (query, newData, option, cb) ->
		cb 201  unless query
		if not cb and newData
			cb = newData
			newData = {}
		_.extend option,
			safe: true

		@getDb().update query, newData, option, (err, u) ->
			cb err, true

	remove: (query, newData, cb) ->
		cb 201  unless query
		if not cb and newData
			cb = newData
			newData = {}
		_.extend newData,
			isDelete: true

		@getDb().update query, { $set: newData }, (err, u) ->
			cb err, true


	hardRemove: (query, opt, cb) ->
		cb 202  unless query
		if not cb and opt
			cb = opt
			opt = {}
		@getDb().remove query, (err, obj) ->
			cb err, true


	find: (param, opt, cb) ->
		cb 202  unless param
		if not cb and opt
			cb = opt
			opt = {}
		_.extend param,
			isDelete: false

		@getDb().find(param, opt).toArray (err, objs) ->
			cb err, objs


	findOne: (param, opt, cb) ->
		cb 202  unless param
		if not cb and opt
			cb = opt
			opt = {}
		_.extend param,
			isDelete: false

		@getDb().findOne param, (err, obj) ->
			cb err, obj


	findAndModify: (query, doc, opt, cb) ->
		cb 202  unless query
		if not cb and opt
			cb = opt
			opt = {}
		_.extend query,
			isDelete: false

		_.extend opt,
			safe: true
			new: true

		@getDb().findAndModify query, {}, doc, opt, (err, obj) ->
			cb err, obj


	findAll: (cb) ->
		param = isDelete: false
		@getDb().find(param).toArray (err, objs) ->
			cb err, objs


	allKey: (cb) ->
		param = isDelete: false
		@getDb().find(param).toArray (err, objs) ->
			keys = []
			_.each objs, (item) ->
				key = _.keys(item)
				keys = keys.concat(key)

			keys = _.uniq(keys)
			cb err, keys


exports.DB = DB