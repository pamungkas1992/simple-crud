path = require("path")
fs = require("fs")
supportFile = ["js", "coffee"]
module.exports = (app, log, db, config, error, system, crypto, _, uuid, fs, logic, email, jobs) ->
	[].forEach (folder) ->
		files = fs.readdirSync(path.join(__dirname, folder))
		files.forEach (file) ->
			return  unless _.contains(supportFile, file.split(".")[1])
			exports[file.replace(/\.js$/, "").replace(/\.coffee$/, "")] = require(path.join(__dirname, folder, file.replace(/\.js$/, "").replace(/\.coffee$/, "")))(app, log, db, config, error, system, crypto, _, uuid, fs, logic, email, jobs)


	exports