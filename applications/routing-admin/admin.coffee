module.exports = (app, log, db, config, error, system, passport, validate, fs, form, adapter, logic, email, _, random, async, client, geoip, moment, ua, humanize, jobs) ->

	adminPath = config.adminPath
	adminDir = config.adminTemplateDir

	doLogin = (req, res, next) ->
		passport.authenticate("local", (err, user, info) ->
			if err
				req.session.loginError = err
				return next err
			unless user
				req.session.loginError = 'login.error.match'
				return res.redirect "/login"
			req.logIn user, (err) ->
				if err
					req.session.loginError = err?.message or 'login.error.general'
					return next err
				if user.isSuperAdmin
					res.redirect "/dashboard"
				else
					res.redirect "/dashboard?user"

		) req, res, next


	app.get "#{adminPath}", passport.ensureAuthenticated, (req, res, next) ->
		res.render "#{adminDir}/table-datatable",
			adminUrl: adminPath

	app.get "#{adminPath}/login", (req, res, next) ->
		if req.user
			return res.redirect "#{adminPath}"
		res.render "#{adminDir}/login",
			adminUrl: adminPath

	app.post "#{adminPath}/login", (req, res, next) ->
		if not req.body.username or not req.body.password
			req.session.loginError = 'login.error.empty'
			return res.redirect '#{adminPath}/login'
		doLogin req, res, next

	app.get "/logout", (req, res, next) ->
		req.logout()
		res.redirect "/"