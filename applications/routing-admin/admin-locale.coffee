module.exports = (app, log, db, config, error, system, passport, validate, fs, form, adapter, logic, email, _, random, async, client, geoip, moment, ua, humanize, jobs) ->

	adminPath = config.adminPath
	adminDir = config.adminTemplateDir

	app.get "#{adminPath}/locale", (req, res, next) ->
		if config.locales.length is 1
			return res.redirect "#{adminPath}/locale/view/#{config.locales[0]}"
		res.render "#{adminDir}/config/locale",
			locales: config.locales or []
			adminUrl: adminPath

	app.get "#{adminPath}/locale/view/:locale", (req, res, next) ->
		{ locale } = req.params
		logic.locale.get locale, (err, content) ->
			res.render "#{adminDir}/config/locale",
				content: content or ''
				locale: locale
				adminUrl: adminPath

	app.post "#{adminPath}/locale/general/save/:locale", (req, res, next) ->
		{ locale } = req.params
		{ content } = req.body
		logic.locale.save locale, content, (err) ->
			if err
				res.send 500
			else
				res.send 200
