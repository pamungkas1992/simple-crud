module.exports = (app, log, db, config, error, system, passport, validate, fs, form, adapter, logic, email, _, random, async, client, geoip, moment, ua, humanize, jobs) ->

	adminPath = config.adminPath
	adminDir = config.adminTemplateDir

	app.get "#{adminPath}/config", (req, res, next) ->
		logic.config.get (err, config) ->
			if err
				log.error error[err]
				return res.redirect '/?error=true'
				
			res.render "#{adminDir}/config/config",
				metadata: config?.metadata or JSON.stringify({})
				adminUrl: adminPath

	app.post "#{adminPath}/config/general/save", (req, res, next) ->
		if req.body.metadata
			logic.config.save req.body.metadata, (err, config) ->
				res.send 200
