module.exports = (app, log, db, config, error, system, passport, validate, fs, form, adapter, logic, email, _, random, async, client, geoip, moment, ua, humanize) ->

	adminPath = config.adminPath
	adminDir = config.adminTemplateDir

	app.get "#{adminPath}/user", passport.ensureAuthenticated, (req, res, next) ->
		logic.user.list (err, result) ->
			if err
				log.error error[err]
				res.render "#{adminDir}/dashboard-user",
					message: error[err]
					session: req.session
					contents: []
					user: req.user
				return
			res.render "#{adminDir}/dashboard-user",
				contents: result
				adminUrl: adminPath

	app.get "#{adminPath}/user/add", passport.ensureAuthenticated, (req, res, next) ->
		if req.session.form
			form = req.session.form
			req.session.form = false
		else
			form = false
		res.render "#{adminDir}/form-user",
			adminUrl: adminPath
			form: form

	app.post "#{adminPath}/user/add", passport.ensureAuthenticated, (req, res, next) ->
		{ name, email, password } = req.body
		validate = () ->
			error = {}
			if not email
				error.email = true
			if not name
				error.name = true
			error
		error = validate()
		if _.isEmpty error
			logic.user.register req.body, (err, user) ->
				if err or not user
					log.error err
					req.session.error = 'registration-failed'
					req.session.form = req.body
					return res.redirect "#{adminPath}/user/add"
				req.session.message = { saved: true }
				res.redirect "#{adminPath}/user"
		else
			req.session.error = 'registration-failed'
			req.session.form = req.body
			return res.redirect "#{adminPath}/user/add"

	app.get "#{adminPath}/user/edit/:id", passport.ensureAuthenticated, (req, res, next) ->
		id = req.param 'id'
		logic.user.findOne id, (err, user) ->
			if err or not user
				log.error err
				res.redirect "#{adminPath}/user?edited=fail"

			res.render "#{adminDir}/form-user",
				adminUrl: adminPath
				form: user

	app.post "#{adminPath}/user/edit/:id", passport.ensureAuthenticated, (req, res, next) ->
		id = req.param 'id'
		{ name, email, password } = req.body
		validate = () ->
			error = {}
			if not email
				error.email = true
			if not name
				error.name = true
			error
		error = validate()
		if _.isEmpty error
			logic.user.update id, req.body, (err, user) ->
				if err or not user
					log.error err
					req.session.error = 'registration-failed'
					req.session.form = req.body
					return res.redirect "#{adminPath}/user/edit/#{id}"
				req.session.message = { saved: true }
				res.redirect "#{adminPath}/user"
		else
			req.session.error = 'registration-failed'
			req.session.form = req.body
			return res.redirect "#{adminPath}/user/edit/#{id}"

	app.get "#{adminPath}/user/delete/:id", passport.ensureAuthenticated, (req, res, next) ->
		id = req.param 'id'
		logic.user.remove id, (err, user) ->
			if err
				log.error err
			req.session.message = { deleted: true }
			res.redirect "#{adminPath}/user"