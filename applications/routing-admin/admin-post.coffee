module.exports = (app, log, db, config, error, system, passport, validate, fs, form, adapter, logic, email, _, random, async, client, geoip, moment, ua, humanize) ->
	crypto = require 'crypto'
	adminPath = config.adminPath
	adminDir = config.adminTemplateDir

	allowedFormat = [ 'image/png', 'image/jpeg' ]
	getFolder = (folder) -> 
					"#{__dirname}/../../public-assets/#{folder}/"
	hash = (text) -> 
				crypto.createHash('sha256').update(text + config.photoHash).digest('hex')
	maxSize = 1024 * 1024 * 3 #3MB

	app.get "#{adminPath}/post", passport.ensureAuthenticated, (req, res, next) ->
		logic.post.list (err, result) ->
			if err
				log.error error[err]
				res.render "#{adminDir}/dashboard-post",
					message: error[err]
					session: req.session
					contents: []
					user: req.user
				return
			res.render "#{adminDir}/dashboard-post",
				contents: result
				adminUrl: adminPath

	app.get "#{adminPath}/post/add", passport.ensureAuthenticated, (req, res, next) ->
		if req.session.form
			form = req.session.form
			req.session.form = false
		else
			form = false
		res.render "#{adminDir}/form-post",
			adminUrl: adminPath
			form: form

	app.post "#{adminPath}/post/add", passport.ensureAuthenticated, (req, res, next) ->
		{ title, desc } = req.body
		validate = () ->
			error = {}
			if not title
				error.title = true
			if not desc
				error.desc = true
			error
		error = validate()
		if _.isEmpty error
			if req.files?
				{ img } = req.files
			try
				delete req.body.__proto__
			if img?.size > 0
				unless img
					return res.send 500,
						error: "file-empty"
				unless img.type in allowedFormat
					return res.send 500,
						error: "format-not-allowed"

				unless 1 < img.size <= maxSize
					return res.send 500,
						error: "size-too-big"

				file = hash("#{img.name}#{Date.now()}")
				folder = getFolder 'images'

				if img.type is 'image/png'
					ext = '.png'
				else
					ext = '.jpg'
				file = "#{file}#{ext}"
				fs.mkdirs folder, (err) ->
					if err
						log.error "Failed to create folder. Error:#{err}. Path: #{folder}"

					fs.rename img.path, "#{folder}#{file}", (err) ->
						if err
							log.error "Failed to move file. Error:#{err}. Path: #{folder}#{file}"
						_.extend req.body,
							image:
								folder: '/public/images/'
								file: file
						logic.post.register req.body, (err, post) ->
							if err or not post
								log.error err
								req.session.error = 'registration-failed'
								req.session.form = req.body
								return res.redirect "#{adminPath}/post/add"
							req.session.message = { saved: true }
							res.redirect "#{adminPath}/post"
			else
				logic.post.register req.body, (err, post) ->
					if err or not post
						log.error err
						req.session.error = 'registration-failed'
						req.session.form = req.body
						return res.redirect "#{adminPath}/post/add"
					req.session.message = { saved: true }
					res.redirect "#{adminPath}/post"
		else
			req.session.error = 'registration-failed'
			req.session.form = req.body
			return res.redirect "#{adminPath}/post/add"

	app.get "#{adminPath}/post/edit/:id", passport.ensureAuthenticated, (req, res, next) ->
		id = req.param 'id'
		logic.post.findOne id, (err, post) ->
			if err or not post
				log.error err
				res.redirect "#{adminPath}/post?edited=fail"

			res.render "#{adminDir}/form-post",
				adminUrl: adminPath
				form: post

	app.post "#{adminPath}/post/edit/:id", passport.ensureAuthenticated, (req, res, next) ->
		id = req.param 'id'
		{ title, desc } = req.body
		validate = () ->
			error = {}
			if not title
				error.title = true
			if not desc
				error.desc = true
			error
		error = validate()
		if _.isEmpty error
			if req.files?
				{ img } = req.files
			try
				delete req.body.__proto__
			if img?.size > 0
				unless img
					return res.send 500,
						error: "file-empty"
				unless img.type in allowedFormat
					return res.send 500,
						error: "format-not-allowed"

				unless 1 < img.size <= maxSize
					return res.send 500,
						error: "size-too-big"

				file = hash("#{img.name}#{Date.now()}")
				folder = getFolder 'images'

				if img.type is 'image/png'
					ext = '.png'
				else
					ext = '.jpg'
				file = "#{file}#{ext}"
				fs.mkdirs folder, (err) ->
					if err
						log.error "Failed to create folder. Error:#{err}. Path: #{folder}"

					fs.rename img.path, "#{folder}#{file}", (err) ->
						if err
							log.error "Failed to move file. Error:#{err}. Path: #{folder}#{file}"
						_.extend req.body,
							image:
								folder: '/public/images/'
								file: file
						logic.post.update id, req.body, (err, post) ->
							if err or not post
								log.error err
								req.session.error = 'registration-failed'
								req.session.form = req.body
								return res.redirect "#{adminPath}/post/edit/#{id}"
							req.session.message = { saved: true }
							res.redirect "#{adminPath}/post"
			else
				logic.post.update id, req.body, (err, post) ->
					if err or not post
						log.error err
						req.session.error = 'registration-failed'
						req.session.form = req.body
						return res.redirect "#{adminPath}/post/edit/#{id}"
					req.session.message = { saved: true }
					res.redirect "#{adminPath}/post"
		else
			req.session.error = 'registration-failed'
			req.session.form = req.body
			return res.redirect "#{adminPath}/post/edit/#{id}"

	app.get "#{adminPath}/post/delete/:id", passport.ensureAuthenticated, (req, res, next) ->
		id = req.param 'id'
		logic.post.remove id, (err, post) ->
			if err
				log.error err
			req.session.message = { deleted: true }
			res.redirect "#{adminPath}/post"