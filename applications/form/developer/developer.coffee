module.exports = (app, log, db, config, passport, validate, fs, forms, jobs) ->
	fields = forms.fields
	validators = forms.validators
	widgets = forms.widgets
	exports.register = (cb) ->
		form = forms.create(
			first_name: fields.string(
				required: true
				widget: widgets.text(placeholder: "First Name")
			)
			last_name: fields.string(
				required: true
				widget: widgets.text(placeholder: "Last Name")
			)
			email: fields.email(
				required: true
				widget: widgets.text(placeholder: "Email")
			)
			password: fields.password(
				required: true
				widget: widgets.password(placeholder: "Password")
			)
		)
		cb form

	exports.login = (cb) ->
		form = forms.create(
			email: fields.email(
				required: true
				widget: widgets.text(placeholder: "Email")
			)
			password: fields.password(
				required: true
				widget: widgets.password(placeholder: "Password")
			)
		)
		cb form

	exports.forget = (cb) ->
		form = forms.create(email: fields.email(
			required: true
			widget: widgets.text(placeholder: "Email")
		))
		cb form

	exports.change_password = (cb) ->
		form = forms.create(
			code: fields.string(
				required: true
				widget: widgets.text()
			)
			password: fields.password(
				required: true
				widget: widgets.password(placeholder: "Password")
			)
		)
		cb form

	exports.newApp = (cb) ->
		form = forms.create(
			application_name: fields.string(
				required: true
				widget: widgets.text(placeholder: "Application name")
				help_text: "Your application name."
			)
			description: fields.string(
				required: true
				widget: widgets.text(placeholder: "Description")
				help_text: "Your application description."
			)
			website: fields.string(
				required: true
				widget: widgets.text(placeholder: "Website")
				help_text: "Your application's publicly accessible home page."
			)
		)
		cb form

	exports.newAppCollection = (cb) ->
		form = forms.create(name: fields.string(
			required: true
			widget: widgets.text(placeholder: "Collection Name")
		))
		cb form

	exports