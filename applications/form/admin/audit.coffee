module.exports = (app, log, db, config, passport, validate, fs, forms, logic) ->
	fields = forms.fields
	validators = forms.validators
	widgets = forms.widgets
	exports.create = (cb, req) ->
		form = forms.create(
			from: fields.string(
				id: "from"
				required: true
				widget: widgets.text(
					placeholder: "Date From"
					classes: ["from"]
				)
			)
			to: fields.string(
				id: "to"
				required: true
				widget: widgets.text(
					placeholder: "Date To"
					classes: ["to"]
				)
			)
		)
		cb form

	exports