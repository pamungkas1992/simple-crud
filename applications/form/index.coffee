path = require("path")
fs = require("fs")
supportFile = ["js", "coffee"]
_ = require("underscore")
module.exports = (app, log, db, config, passport, validate, fs, forms, jobs) ->
	["admin", "developer"].forEach (folder) ->
		files = fs.readdirSync(path.join(__dirname, folder))
		files.forEach (file) ->
			return  unless _.contains(supportFile, file.split(".")[1])
			exports[file.replace(/\.js$/, "").replace(/\.coffee$/, "")] = require(path.join(__dirname, folder, file.replace(/\.js$/, "").replace(/\.coffee$/, "")))(app, log, db, config, passport, validate, fs, forms, jobs)

	exports