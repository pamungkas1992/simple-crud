module.exports =
	common:
		port:
			frontend: 8000
			api: 8808
			kue: 8800

		debug: true
		serverName: "Simple Crud" ## server name chage this asap
		serverVersion: "0.0.1"
		cookieSecret: "cookieSimpleCrud"
		sessionSecret: "sessionSimpleCrud"
		photoHash: "photoCrud"
		template: "simplecrud" 
		templateDir: "themes/simplecrud" #define public template dir
		adminTemplate: "adminre" #template name
		adminTemplateDir: "themes-admin/adminre" ##define folder
		adminPath: "/dashboard" ##admin url
		locales: [
			"id"
		]
		defaultLocale: 'id'
		superUsers: {
			'pamungkas1992@gmail.com' : '6c27e88d3bc57e8bad6c7fe6e5127dddcc43e49c90a6931e64150e04499efd6c', #ask me for password
		}
		mandrill:
			API_key: "SWR7LTmNNGKLQIjew_ieYQ"

		sendGrid:
			server: "smtp.sendgrid.net"
			username: "pamungkas1992"
			password: "p1p2p3p4p5"

		redis:
			server: "127.0.0.1"
			port: 6379
			db: "simple_crud"
			pass: ""
			prefix: "sess:frefixname:" ##please change prefixname

		mongodb:
			server: "localhost"
			port: 27017
			dbname: "simplecrud"
			options: "auto_reconnect"

		api_log:
			defaultFile: "log/api_log.log"
			exceptionsFile: "log/api_exceptions.log"
			mongodb:
				dbname: "simplecrud_api_log" #please change db name

		log:
			defaultFile: "log/log.log"
			exceptionsFile: "log/exceptions.log"
			mongodb:
				dbname: "simplecrud_log" #please change db name

	development: {}
	test: {}
	production: {}