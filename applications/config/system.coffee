module.exports =
	common:
		user:
			type:
				normal: "normal"
				cloud: "cloud"
				security: "security"
				admin: "admin"
				superAdmin: "superAdmin"

			status:
				pending: "pending"
				active: "active"
				ban: "ban"

		request:
			test: "test"
			ping: "ping"
			collection: "collection"
			blob: "blob"

	development: {}
	test: {}
	production: {}