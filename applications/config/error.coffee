module.exports =
	common:
		100: "Parameter data is not supplied"
		101: "Parameter is not Base64"
		102: "Parameter is not JSON"
		301: "File not found"
		404: "page not found"
		501: "Can't find email address"

	development:
		dummy: {}

	test:
		dummy: {}

	production:
		dummy: {}