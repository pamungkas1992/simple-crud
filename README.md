Application Name
=============
[Project description]

Prerequisites:
---------------------
- Node.js
    -  Download: http://nodejs.org/download/
- Redis
    - Download: http://redis.io/download
- Mongodb
    - Download: http://www.mongodb.org/downloads
- make sure redis and mongodb run on default port


General Note
---------------------
Please install node-dev for development use
npm install -g node-dev

Config
---------------------
- Open at the under config folder

Locale
---------------------
- under folder locales. Rename xx.js.example to xx.js

Run at Development Environment
---------------------
- Run at console: node-dev app.coffee

Run at Production Environment
---------------------
- Run at console: forever app.js
- Run at console: forever server.js


Folder Structure
---------------------
- applications/adapter
    - http://en.wikipedia.org/wiki/Adapter_pattern
- applications/config
    -  system config, error mapping, general config (such as server port, dbname, etc)
- applications/email/email-template
    - html and text template for email that sent to users
- applications/form
    - backend side form model, this model will automatically generate to HTML bootstrap form
- applications/email/mail-logic
    - sending mail code
- applications/modules
    - all about function inside system
- locales
    - i18n text
- log
    - where diamonds are found.
- node_modules
    - project dependency 
- public-assets
    - all static assets goes here
- applications/routing-public
    - place for define anywhere public can access
- applications/routing-admin
    - only autenticated user can see everything inside this url
- views
    - HTML views for the website