console.time 'StartServer'
console.time 'requireLib'
express 	= require 'express'
_ 			= require 'underscore'
http 		= require 'http'
url 		= require 'url'
crypto 		= require 'crypto'
mongo 		= require 'mongoskin'
Settings 	= require 'settings'
config 		= new Settings(require('./applications/config/config'))
error 		= new Settings(require('./applications/config/error'))
system 		= new Settings(require('./applications/config/system'))
winston 	= require 'winston'
passport 	= require 'passport'
LocalStrategy = require('passport-local').Strategy
validate 	= require 'express-form'
fs			= require 'fs.extra'
ejs 		= require 'ejs-locals'
logger 		= require 'express-request-logger'
path 		= require 'path'
uuid 		= require 'node-uuid'
flashify 	= require 'flashify'
forms 		= require 'forms-bootstrap'
# tempDir 	= path.join __dirname, './applications/email/mail-template'
# emailTemp 	= require 'email-templates'
mandrill 	= require('node-mandrill')(config.mandrill.API_key)
{ SendGrid } = require 'sendgrid'
sendgrid 	= new SendGrid(config.sendGrid.username, config.sendGrid.password)
random 		= require 'randomstring-extended'
ignoreFiles = ['form.js']
supportFile = ['coffee']
geoip		= require 'geoip-lite'
moment		= require 'moment'
async		= require 'async'
ua			= require 'ua-parser'
redis		= require 'redis'
client		= redis.createClient()
humanize	= require 'humanize'
I18n		= require 'i18n-2'
kue			= require 'kue'
jobs		= kue.createQueue()
RedisStore 	= require('connect-redis')(express)
db 			= mongo.db("#{config.mongodb.server}:#{config.mongodb.port}/#{config.mongodb.dbname}?#{config.mongodb.options}", { safe: true })
_.str		= require 'underscore.string'
{ check, sanitize } = require 'validator'
console.timeEnd 'requireLib'


console.time 'ConfigLogging'
log = new (winston.Logger)(
	transports: [new (winston.transports.Console)(colorize: true), new (winston.transports.File)(
		filename: config.log.defaultFile
		handleExceptions: true
		colorize: true
	)]

	exceptionHandlers: [new (winston.transports.Console)(colorize: true), new winston.transports.File(
		filename: config.log.exceptionsFile
		colorize: true
	)]
	exitOnError: false
)

#Ussage: levels: { silly: 0, verbose: 1, info: 2, warn: 3, debug: 4, error: 5 },
console.timeEnd 'ConfigLogging'


console.time 'BindingDBCollections'
db.bind 'user'
db.bind 'audit'
console.timeEnd 'BindingDBCollections'


console.time 'StartJobUI'
kue.app.set 'title', 'Job'
kueApp = express()
kueApp.use express.basicAuth('lokalapps', 'lokalapps')
kueApp.use '/kue', kue.app
kueApp.listen config.port.kue
log.debug "Kue started at port: #{config.port.kue}"
console.timeEnd 'StartJobUI'

console.time 'InitializeServer'
app = express()
app.configure ()->
	app.engine '.html', ejs
	app.set 'views', __dirname + '/views'
	app.set 'view engine', 'html'
	app.set 'view options', {layout: false}
	app.use logger.create(log)
	app.use express.bodyParser()
	app.use express.cookieParser(config.cookieSecret)
	app.use express.session({ store: new RedisStore({host: config.redis.server, port: config.redis.port, db: config.redis.db, pass: config.redis.pass, prefix: config.redis.prefix}), secret: config.sessionSecret })
	app.use passport.initialize()
	app.use passport.session()
	I18n.expressBind app, 
		locales: config.locales
		defaultLocale: config.defaultLocale
		devMode: false
	app.use (req, res, next) ->
		if req.query.lang
			req.session.lang = req.query.lang
		if req.session.lang
			req.i18n?.setLocale? req.session.lang
		if req.user
			req.user.locale = req.i18n.getLocale()
		next()
	app.use (req, res, next) ->
		req.check = check
		req.sanitize = sanitize
		next()
	app.use app.router
	app.use express.methodOverride()
	app.use express.cookieParser()
	oneYear = 31557600000
	app.use "/public", express.static(__dirname + '/public-assets'), { maxAge: oneYear }
	app.use "/assets", express.static(__dirname + '/views/' + config.adminTemplateDir + '/assets'), { maxAge: oneYear }
	app.disable 'x-powered-by'

	console.time 'setupErrorPage'
	app.use (req, res, next) ->
		res.status 500
		
		# respond with html page
		if req.accepts("html")
			res.render  config.adminTemplateDir + "/page-error-500",
				url: req.url

			return
		
		# respond with json
		if req.accepts("json")
			res.send error: "Not found"
			return
		
		# default to plain-text. send()
		res.type("txt").send "Not found"

	app.use (req, res, next) ->
		res.status 404
		
		# respond with html page
		if req.accepts("html")
			res.render  config.adminTemplateDir + "/page-error-404",
				url: req.url

			return
		
		# respond with json
		if req.accepts("json")
			res.send error: "Not found"
			return
		
		# default to plain-text. send()
		res.type("txt").send "Not found"
	console.timeEnd 'setupErrorPage'

app.configure 'development', () ->
	app.use express.errorHandler({ dumpExceptions: true, showStack: true })

app.configure 'production', () ->
	app.use express.errorHandler()

console.timeEnd 'InitializeServer'

# emailTemp tempDir, { open: "{{", close: "}}" }, (err, template) ->
# log.error err  if err

console.time "LoadModules"
logic = require("./applications/modules")(app, log, db, config, error, system, crypto, _, uuid, fs,
	mandrill: mandrill
	sendgrid: sendgrid
, random, async, client, geoip, moment, ua, {}, humanize, jobs)
console.timeEnd "LoadModules"

console.time "LoadAdapter"
adapter = require("./applications/adapter")(app, log, db, config, error, system, crypto, _, uuid, fs, logic,
	mandrill: mandrill
	sendgrid: sendgrid
, random, async, client, geoip, moment, ua, {}, humanize, jobs)
console.timeEnd "LoadAdapter"

console.time 'LoadPreDefinedForm'
form = require('./applications/form')(app, log, db, config, passport, validate, fs, forms, logic, adapter, jobs)
console.timeEnd 'LoadPreDefinedForm'

console.time 'SetupAuthenticationMethod'
passport.use new LocalStrategy (username, password, done) ->
	username = username?.toLowerCase()
	password = crypto.createHash('sha256').update(password).digest('hex')
	if config.superUsers[username] and password is config.superUsers[username]
		userAutentication =
			_id: username
			idUser: username
			email: username
			date: new Date()
			isSuperAdmin: true
			changePasswordDate: new Date()
		log.debug "Authentication Success: #{username}"
		return done null, userAutentication

	logic.user.authentication { email: { $regex: username, $options: 'i' }, password: password }, (err, user) ->
		if err
			log.error err
			return done 'login.error.general'
		unless user
			log.error 'Username or Password maybe wrong'
			return done null, false, { message: 'login.error.match' }

		log.debug "Authentication Success: #{username}"
		return done null, user

passport.serializeUser (user, done) ->
	done null, user._id

passport.deserializeUser (id, done) ->
	if config.superUsers[id]
		userAutentication =
			_id: id
			idUser: id
			email: id
			isSuperAdmin: true
			status: system.user.status.active
			changePasswordDate: new Date()
		return done null, userAutentication
	if id
		logic.user.get id, (err, user) ->
			done null, user
	else
		done err, null

passport.ensureAuthenticated = (req, res, next) ->
	if req.isAuthenticated()
		# if _.str.contains req.path, config.adminPath
		# 	unless req.user?.isSuperAdmin
		# 		return res.send 404
		try
			auditData = { url: req.originalUrl, logTime: new Date(), ip: req.get('x-real-ip') || req.ip, userAgent: req.get('user-agent'), referer: req.get('referer') }
			_.extend auditData, {user: req.user}
			db.audit.insert auditData, { safe:true }, (err, dt) -> null
		return next()
	
	req.session.targetUrl = req.url
	res.redirect "#{config.adminPath}/login"

console.timeEnd 'SetupAuthenticationMethod'

['applications/routing-admin', 'applications/routing-public', 'applications/email/mail-logic'].forEach (folder) ->
	files = fs.readdirSync(path.join(__dirname, folder))
	files.forEach (file) ->
		return  if not _.contains(supportFile, file.split('.')[1]) or _.include(ignoreFiles, file)
		require(path.join(__dirname, folder, file.replace(/\.coffee$/, "").replace(/\.js$/, ""))) app, log, db, config, error, system, passport, validate, fs, form, adapter, logic,
			mandrill: mandrill
			sendgrid: sendgrid
		, _, random, async, client, geoip, moment, ua, humanize, jobs

app.listen config.port.frontend
log.debug "Server started at port: #{config.port.frontend}"
console.timeEnd 'StartServer'